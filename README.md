# Voting Session - Gisomar

This project was developed with the following technologies:
- Spring Boot Starter
- Swagger-Ui
- Lombok
- PostgresSQL
- h2database

## Heroku deploy

[Heroku deploy](https://votingsession.herokuapp.com/swagger-ui/)

## docker postgres

- docker-compose.yml

## Configure your database

    username: postgres
    password: postgres

## How to compile and build this application

    mvn clean install package

The maven is available [maven](https://mvnrepository.com/)

## How to start this application

    mvn spring-boot:run

## How to start this application in mode debugger

    mvn spring-boot:run -Dspring-boot.run.fork=false

## Postman - Integrated Test

In this directory is available the file: [Voting Session.postman_collection.json]: /Voting Session.postman_collection.json
