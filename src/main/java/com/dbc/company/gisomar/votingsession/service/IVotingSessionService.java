package com.dbc.company.gisomar.votingsession.service;

import com.dbc.company.gisomar.api.v1.resource.ValidationApiResource;
import com.dbc.company.gisomar.api.v1.resource.VotingSessionResource;
import com.dbc.company.gisomar.votingsession.entity.VotingSession;

import java.time.LocalDateTime;
import java.util.UUID;

public interface IVotingSessionService {

    /**
     * Verify if the Session is closed by {@link UUID votingSessionId}
     * @param votingSessionId
     * @return
     */
    ValidationApiResource isVotingSessionClosed(UUID votingSessionId);

    /**
     * Find the Voting Session by {@link UUID id}
     * @param id
     * @return
     */
    VotingSession findById(UUID id);

    /**
     * Create the Voting Session by {@link VotingSessionResource votingSessionResource}
     * @param votingSessionResource
     * @return
     */
    VotingSession create(VotingSessionResource votingSessionResource);

    /**
     * Adding +1 minute by {@link LocalDateTime endVote}
     * @param endVote
     * @return
     */
    LocalDateTime addDateTimeDefaultOneMinute(LocalDateTime endVote);

    /**
     * Verify if the Voting Session not closed by {@link UUID votingSessionId}
     * @param votingSessionId
     * @return
     */
    ValidationApiResource verifyVotingSessionNotClosed(UUID votingSessionId);
}
