package com.dbc.company.gisomar.votingsession.entity;

import com.dbc.company.gisomar.schedule.entity.Schedule;
import com.dbc.company.gisomar.vote.entity.Vote;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class VotingSession {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idVotingSession", unique = true, nullable = false)
    private UUID id;

    @Column(nullable = false)
    private LocalDateTime endVote;

    @ManyToOne(fetch = FetchType.LAZY)
    private Schedule schedule;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Vote> votes;

}
