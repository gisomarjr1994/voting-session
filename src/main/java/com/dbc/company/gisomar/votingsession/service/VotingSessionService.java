package com.dbc.company.gisomar.votingsession.service;

import com.dbc.company.gisomar.api.v1.resource.ValidationApiResource;
import com.dbc.company.gisomar.api.v1.resource.VotingSessionResource;
import com.dbc.company.gisomar.schedule.entity.Schedule;
import com.dbc.company.gisomar.votingsession.entity.VotingSession;
import com.dbc.company.gisomar.votingsession.repository.VotingSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.UUID;

@Component
public class VotingSessionService implements IVotingSessionService {

    private static final String VOTING_SESSION_CLOSED = "Voting Session is closed";
    private static final String VOTING_SESSION_NOT_REGISTERED = "Voting Session is not already registered";
    private static final String VOTING_SESSION_NOT_CLOSED = "Voting Session is not already closed";
    private static final String ZONE = "America/Sao_Paulo";

    @Autowired
    private VotingSessionRepository votingSessionRepository;

    @Override
    public ValidationApiResource isVotingSessionClosed(UUID votingSessionId) {
        var votingSessionOptional = this.votingSessionRepository.findById(votingSessionId);

        if(!votingSessionOptional.isPresent()) {
            return new ValidationApiResource(HttpStatus.NOT_FOUND, VOTING_SESSION_NOT_REGISTERED, false);
        }
        var votingSession = votingSessionOptional.get();

        if(votingSession.getEndVote().isBefore(LocalDateTime.now(ZoneId.of(ZONE)))) {
            return new ValidationApiResource(HttpStatus.BAD_REQUEST, VOTING_SESSION_CLOSED, false);
        }
        return new ValidationApiResource(true);
    }

    @Override
    public VotingSession findById(UUID id) {
       var votingSessionOptional = votingSessionRepository.findById(id);
       if (!votingSessionOptional.isPresent()) {
           return null;
       }
       return votingSessionOptional.get();
    }

    @Override
    public VotingSession create(VotingSessionResource votingSessionResource) {
        VotingSession votingSession = new VotingSession();
        votingSession.setEndVote(votingSessionResource.getEndVote());
        Schedule schedule = new Schedule();
        schedule.setId(votingSessionResource.getScheduleId());
        votingSession.setSchedule(schedule);
        return this.votingSessionRepository.save(votingSession);
    }

    @Override
    public LocalDateTime addDateTimeDefaultOneMinute(LocalDateTime endVote) {
        if (endVote == null) {
            return LocalDateTime.now(ZoneId.of(ZONE)).plusMinutes(1);
        }
        return endVote;
    }

    @Override
    public ValidationApiResource verifyVotingSessionNotClosed(UUID votingSessionId) {
        var votingSessionOptional = this.votingSessionRepository.findById(votingSessionId);
        if (!votingSessionOptional.isPresent()) {
            return new ValidationApiResource(HttpStatus.BAD_REQUEST, VOTING_SESSION_NOT_REGISTERED, false);
        }
        var votingSession = votingSessionOptional.get();
        if(votingSession.getEndVote().isAfter(LocalDateTime.now(ZoneId.of(ZONE)))) {
            return new ValidationApiResource(HttpStatus.BAD_REQUEST, VOTING_SESSION_NOT_CLOSED, false);
        }
        return new ValidationApiResource(true);
    }
}
