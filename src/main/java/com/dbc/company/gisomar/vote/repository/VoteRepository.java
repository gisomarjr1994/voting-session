package com.dbc.company.gisomar.vote.repository;

import com.dbc.company.gisomar.vote.entity.Vote;
import com.dbc.company.gisomar.vote.enums.Choice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface VoteRepository extends JpaRepository<Vote, UUID> {

    Optional<Vote> findByAssociateIdAndVotingSessionId(
            @Param("idAssociate") UUID idAssociate,
            @Param("idVotingSession") UUID idVotingSession);

    Integer countByVotingSessionIdAndChoice(
            @Param("idVotingSession") UUID idVotingSession,
            @Param("choice")Choice choice);
}
