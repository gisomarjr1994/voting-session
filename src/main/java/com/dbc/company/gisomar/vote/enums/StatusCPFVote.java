package com.dbc.company.gisomar.vote.enums;

public enum StatusCPFVote {
    ABLE_TO_VOTE,
    UNABLE_TO_VOTE
}
