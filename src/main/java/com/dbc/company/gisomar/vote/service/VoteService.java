package com.dbc.company.gisomar.vote.service;

import com.dbc.company.gisomar.api.v1.resource.AssociateStatusResource;
import com.dbc.company.gisomar.api.v1.resource.ValidationApiResource;
import com.dbc.company.gisomar.api.v1.resource.VoteResource;
import com.dbc.company.gisomar.associate.entity.Associate;
import com.dbc.company.gisomar.associate.repository.AssociateRepository;
import com.dbc.company.gisomar.vote.entity.Vote;
import com.dbc.company.gisomar.vote.enums.Choice;
import com.dbc.company.gisomar.vote.enums.StatusCPFVote;
import com.dbc.company.gisomar.vote.repository.VoteRepository;
import com.dbc.company.gisomar.votingsession.entity.VotingSession;
import com.dbc.company.gisomar.votingsession.repository.VotingSessionRepository;
import com.dbc.company.gisomar.votingsession.service.VotingSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.util.UUID;

@Component
public class VoteService implements IVoteService {

    private static final String VOTING_SESSION_NOT_REGISTERED = "Voting Session is not already registered";
    private static final String ASSOCIATE_NOT_REGISTERED = "Associate is not already registered";
    private static final String ASSOCIATE_ALREADY_VOTED = "Associate already voted";
    private static final String ASSOCIATE_UNABLE_TO_VOTE = "Associate unable to vote";
    private static final String URL = "https://user-info.herokuapp.com/users/";

    @Autowired
    private VoteRepository voteRepository;

    @Autowired
    private VotingSessionRepository votingSessionRepository;

    @Autowired
    private AssociateRepository associateRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private VotingSessionService votingSessionService;

    @Override
    public Vote create(VoteResource voteResource) {
        Vote vote = new Vote();
        Associate associate = new Associate();
        associate.setId(voteResource.getAssociateId());
        VotingSession votingSession = new VotingSession();
        votingSession.setId(voteResource.getVoteSessionId());
        vote.setChoice(voteResource.getChoice());
        vote.setAssociate(associate);
        vote.setVotingSession(votingSession);
        return this.voteRepository.save(vote);
    }

    @Override
    public ValidationApiResource existsVotingSession(UUID id) {
        var optionalVotingSession = this.votingSessionRepository.findById(id);
        if (!optionalVotingSession.isPresent()) {
            return new ValidationApiResource(HttpStatus.NOT_FOUND, VOTING_SESSION_NOT_REGISTERED, false);
        }
        return new ValidationApiResource(true);
    }

    @Override
    public ValidationApiResource existsAssociate(UUID id) {
        var associateOptional = this.associateRepository.findById(id);
        if(!associateOptional.isPresent()) {
            return new ValidationApiResource(HttpStatus.NOT_FOUND, ASSOCIATE_NOT_REGISTERED, false);
        }
        return new ValidationApiResource(true);
    }

    @Override
    public ValidationApiResource isAssociateUnableVote(String cpf) {
        var associateStatusResource = restTemplate.getForObject(URL + cpf, AssociateStatusResource.class);
        if(associateStatusResource == null || StatusCPFVote.UNABLE_TO_VOTE.equals(associateStatusResource.getStatus())) {
            return new ValidationApiResource(HttpStatus.BAD_REQUEST, ASSOCIATE_UNABLE_TO_VOTE, false);
        }
        return new ValidationApiResource(true);
    }

    @Override
    public ValidationApiResource isVotingSessionClosed(UUID votingSessionId) {
        return this.votingSessionService.isVotingSessionClosed(votingSessionId);
    }

    @Override
    public ValidationApiResource isAssociateAlreadyVoted(UUID associateId, UUID voteSessionId) {
        var voteAssociateFound = voteRepository.findByAssociateIdAndVotingSessionId(
                associateId, voteSessionId);
        if (voteAssociateFound.isPresent()) {
            return new ValidationApiResource(HttpStatus.BAD_REQUEST, ASSOCIATE_ALREADY_VOTED,false);
        }
        return new ValidationApiResource(true);
    }

    @Override
    public Integer countByVotingSessionIdAndChoice(UUID votingSessionId, Choice choice) {
        return this.voteRepository.countByVotingSessionIdAndChoice(votingSessionId, choice);
    }
}
