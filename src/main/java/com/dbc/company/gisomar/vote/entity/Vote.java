package com.dbc.company.gisomar.vote.entity;
import com.dbc.company.gisomar.associate.entity.Associate;
import com.dbc.company.gisomar.vote.enums.Choice;
import com.dbc.company.gisomar.votingsession.entity.VotingSession;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Vote {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idVote", unique = true, nullable = false)
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column(length = 3, nullable = false)
    private Choice choice;

    @ManyToOne(fetch = FetchType.LAZY)
    private Associate associate;

    @ManyToOne(fetch = FetchType.LAZY)
    private VotingSession votingSession;
}
