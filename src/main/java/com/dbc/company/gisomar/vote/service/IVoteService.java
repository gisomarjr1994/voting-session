package com.dbc.company.gisomar.vote.service;

import com.dbc.company.gisomar.api.v1.resource.ValidationApiResource;
import com.dbc.company.gisomar.api.v1.resource.VoteResource;
import com.dbc.company.gisomar.vote.entity.Vote;
import com.dbc.company.gisomar.vote.enums.Choice;
import java.util.UUID;

public interface IVoteService {

    /**
     * Create the vote by {@link VoteResource voteResource}
     * @param voteResource
     * @return
     */
    Vote create(VoteResource voteResource);

    /**
     * Verify if exists Voting Session by {@link UUID id}
     * @param id
     * @return
     */
    ValidationApiResource existsVotingSession(UUID id);

    /**
     * Verify if exists Associate by {@link UUID id}
     * @param id
     * @return
     */
    ValidationApiResource existsAssociate(UUID id);

    /**
     * Verify if the Associate is unable for the vote
     * @param cpf
     * @return
     */
    ValidationApiResource isAssociateUnableVote(String cpf);

    /**
     * Verify if the Voting Session is closed by the {@link UUID votingSessionId}
     * @param votingSessionId
     * @return
     */
    ValidationApiResource isVotingSessionClosed(UUID votingSessionId);

    /**
     * Verify if the associate already voted by the {@link UUID associateId}
     * and {@link UUID voteSessionId}
     * @param associateId
     * @param voteSessionId
     * @return
     */
    ValidationApiResource isAssociateAlreadyVoted(UUID associateId, UUID voteSessionId);

    /**
     * Count the Voting Session by the {@link UUID votingSessionId} and {@link Choice choice}
     * @param votingSessionId
     * @param choice
     * @return
     */
    Integer countByVotingSessionIdAndChoice(UUID votingSessionId, Choice choice);

}
