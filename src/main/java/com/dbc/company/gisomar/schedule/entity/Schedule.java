package com.dbc.company.gisomar.schedule.entity;

import lombok.*;
import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Schedule {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idSchedule", unique = true, nullable = false)
    private UUID id;

    @Column(nullable = true)
    private String description;
}
