package com.dbc.company.gisomar.schedule.service;

import com.dbc.company.gisomar.api.v1.resource.ScheduleResource;
import com.dbc.company.gisomar.schedule.entity.Schedule;
import com.dbc.company.gisomar.schedule.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.UUID;

@Service
public class ScheduleService implements IScheduleService {

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Override
    public Schedule findById(UUID id) {
        var schedule = scheduleRepository.findById(id);
        if(!schedule.isPresent()) {
            return null;
        }
        return schedule.get();
    }

    @Override
    public Schedule create(ScheduleResource scheduleResource) {
        Schedule schedule = new Schedule();
        schedule.setDescription(scheduleResource.getDescription());
        return this.scheduleRepository.save(schedule);
    }
}
