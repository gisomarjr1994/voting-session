package com.dbc.company.gisomar.schedule.service;

import com.dbc.company.gisomar.api.v1.resource.ScheduleResource;
import com.dbc.company.gisomar.schedule.entity.Schedule;

import java.util.UUID;

public interface IScheduleService {

    /**
     * Find the Schedule by id {@link UUID id}
     * @param id
     * @return
     */
    Schedule findById(UUID id);

    /**
     * Create the Schedule by {@link ScheduleResource scheduleResource}
     * @param scheduleResource
     * @return
     */
    Schedule create(ScheduleResource scheduleResource);
}
