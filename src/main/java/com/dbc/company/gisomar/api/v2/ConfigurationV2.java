package com.dbc.company.gisomar.api.v2;

import com.dbc.company.gisomar.api.ConfigurationApi;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.function.Predicate;

import static springfox.documentation.builders.PathSelectors.regex;

@Component
public class ConfigurationV2 {

    private static final String PACKAGEV2 = "com.dbc.company.gisomar.api.v2.controller";
    private static final String APIVERSION = "API 2.0";
    private static final Predicate<String> PATH = regex("/v2.0.*/*");

    @Bean
    public Docket apiVersion2() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(APIVERSION)
                .select()
                .apis(RequestHandlerSelectors.basePackage(PACKAGEV2))
                .paths(PATH)
                .build()
                .apiInfo(ConfigurationApi.getApiInfo(APIVERSION));
    }
}
