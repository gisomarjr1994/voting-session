package com.dbc.company.gisomar.api.v1.controller;

import com.dbc.company.gisomar.api.v1.resource.ValidationApiResource;
import com.dbc.company.gisomar.api.v1.resource.ScheduleResource;
import com.dbc.company.gisomar.schedule.service.ScheduleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@Api(value = "schedules", tags = "Schedule")
@RequestMapping("/v1.0/schedules/")
public class ScheduleController {

    private static final String SCHEDULE_NOT_REGISTERED = "Schedule is not already registered";
    private static final String SCHEDULE_REGISTERED = "Schedule is already registered";

    @Autowired
    private ScheduleService scheduleService;

    @ApiOperation(value = "Get the Schedule",
                  notes = "Find Schedule by id",
                  response = ScheduleResource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return the schedule."),
            @ApiResponse(code = 404, message = SCHEDULE_NOT_REGISTERED)
    })
    @GetMapping("{id}")
    public ResponseEntity<Object> findById(@PathVariable("id") UUID id) {
        var schedule = scheduleService.findById(id);
        if(schedule == null) {
            return ResponseEntity.badRequest().body(
                    new ValidationApiResource(HttpStatus.NOT_FOUND, SCHEDULE_NOT_REGISTERED, false)
            );
        }
        return new ResponseEntity<>(new ScheduleResource(schedule), HttpStatus.OK);
    }

    @PostMapping()
    @ApiOperation(value = "Create the Schedule",
            notes = "Create Schedule by ScheduleResource",
            response = ScheduleResource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Return the Schedule created."),
            @ApiResponse(code = 400, message = SCHEDULE_REGISTERED)
    })
    public ResponseEntity<Object> create(@RequestBody ScheduleResource scheduleResource) {
        var schedule = scheduleService.findById(scheduleResource.getId());
        if (schedule != null) {
            return ResponseEntity.badRequest().body(
                    new ValidationApiResource(HttpStatus.BAD_REQUEST, SCHEDULE_REGISTERED, false)
            );
        }

        var scheduleCreated = scheduleService.create(scheduleResource);
        return new ResponseEntity<>(new ScheduleResource(scheduleCreated), HttpStatus.CREATED);
    }
}
