package com.dbc.company.gisomar.api.v1.resource;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@ApiModel(description = "Resource Result for API Rest")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultResource {

    @ApiModelProperty(notes = "Count the yes Choices")
    private Integer countYes;
    @ApiModelProperty(notes = "Count the not Choices")
    private Integer countNot;
}
