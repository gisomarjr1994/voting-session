package com.dbc.company.gisomar.api.v1.controller;

import com.dbc.company.gisomar.api.v1.resource.VoteResource;
import com.dbc.company.gisomar.associate.service.AssociateService;
import com.dbc.company.gisomar.vote.service.VoteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value = "Votes", tags = "Vote")
@RequestMapping("/v1.0/votes/")
public class VoteController {

    @Autowired
    private VoteService voteService;

    @Autowired
    private AssociateService associateService;

    @PostMapping()
    @ApiOperation(value = "Create the Vote",
            notes = "Create Vote by VoteResource",
            response = VoteResource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Return the Vote created."),
            @ApiResponse(code = 404, message = "Schedule not found.")
    })
    public ResponseEntity<Object> create(@RequestBody VoteResource voteResource) {

        var existsVotingSession = this.voteService.existsVotingSession(
                voteResource.getVoteSessionId());
        if(!existsVotingSession.isValid()) {
            return ResponseEntity.badRequest().body(existsVotingSession);
        }

        var validationApiResourceAssociate = this.voteService.existsAssociate(
                voteResource.getAssociateId());
        if (!validationApiResourceAssociate.isValid()) {
            return ResponseEntity.badRequest().body(validationApiResourceAssociate);
        }

        var associate = this.associateService.findById(voteResource.getAssociateId());
        var unableVote = this.voteService.isAssociateUnableVote(associate.getCpf());
        if(!unableVote.isValid()) {
            return ResponseEntity.badRequest().body(unableVote);
        }

        var votingSessionClosedValidate = this.voteService.isVotingSessionClosed(
                voteResource.getVoteSessionId());
        if(!votingSessionClosedValidate.isValid()) {
            return ResponseEntity.badRequest().body(votingSessionClosedValidate);
        }

        var validationAlreadyVoted = this.voteService.isAssociateAlreadyVoted(
                voteResource.getAssociateId(), voteResource.getVoteSessionId());

        if(!validationAlreadyVoted.isValid()) {
            return ResponseEntity.badRequest().body(validationAlreadyVoted);
        }

        var voteCreated = this.voteService.create(voteResource);
        return new ResponseEntity<>(new VoteResource(voteCreated), HttpStatus.CREATED);
    }

}
