package com.dbc.company.gisomar.api.v1;

import com.dbc.company.gisomar.api.ConfigurationApi;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.function.Predicate;

import static springfox.documentation.builders.PathSelectors.regex;

@Component
public class ConfigurationV1 {

    private static final String PACKAGE_V1 = "com.dbc.company.gisomar.api.v1.controller";
    private static final String API_VERSION = "API 1.0";
    private static final Predicate<String> PATH = regex("/v1.0.*/*");

    @Bean
    public Docket apiVersion1() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(API_VERSION)
                .select()
                .apis(RequestHandlerSelectors.basePackage(PACKAGE_V1))
                .paths(PATH)
                .build()
                .apiInfo(ConfigurationApi.getApiInfo(API_VERSION));

    }
}
