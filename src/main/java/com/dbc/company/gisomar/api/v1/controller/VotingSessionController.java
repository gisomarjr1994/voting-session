package com.dbc.company.gisomar.api.v1.controller;

import com.dbc.company.gisomar.api.v1.resource.ValidationApiResource;
import com.dbc.company.gisomar.api.v1.resource.ResultResource;
import com.dbc.company.gisomar.api.v1.resource.VotingSessionResource;
import com.dbc.company.gisomar.schedule.service.ScheduleService;
import com.dbc.company.gisomar.vote.enums.Choice;
import com.dbc.company.gisomar.vote.service.VoteService;
import com.dbc.company.gisomar.votingsession.service.VotingSessionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.UUID;

@RestController
@Api(value = "Voting Sessions", tags = "Voting Session")
@RequestMapping("/v1.0/voting-sessions/")
public class VotingSessionController {

    private static final String VOTING_SESSION_NOT_CLOSED = "Voting Session is not already closed";
    private static final String VOTING_SESSION_NOT_REGISTERED = "Voting Session is not already registered";
    private static final String VOTING_SESSION_REGISTERED = "Voting Session is already registered";

    @Autowired
    private VotingSessionService votingSessionService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private VoteService voteService;

    @ApiOperation(value = "Get the Voting Session",
                  notes = "Find Voting Session by id",
                  response = VotingSessionResource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return the Voting Session."),
            @ApiResponse(code = 404, message = "Voting Session not found.")
    })
    @GetMapping("{id}")
    public ResponseEntity<Object> findById(@PathVariable("id") UUID id) {
        var votingSession = votingSessionService.findById(id);
        if (votingSession == null) {
            return ResponseEntity.badRequest().body(
                    new ValidationApiResource(HttpStatus.NOT_FOUND, VOTING_SESSION_NOT_REGISTERED, false)
            );
        }
        return new ResponseEntity<>(new VotingSessionResource(votingSession), HttpStatus.OK);
    }

    @PostMapping()
    @ApiOperation(value = "Create the Voting Session",
                  notes = "Create Voting Session by VotingSessionResource",
                  response = VotingSessionResource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Return the Voting Session created."),
            @ApiResponse(code = 404, message = "Schedule not found.")
    })
    public ResponseEntity<Object> create(@RequestBody VotingSessionResource votingSessionResource) {

        var schedule = this.scheduleService.findById(votingSessionResource.getScheduleId());

        if (schedule == null) {
            return ResponseEntity.badRequest().body(
                    new ValidationApiResource(HttpStatus.NOT_FOUND, VOTING_SESSION_REGISTERED, false)
            );
        }

        var newDateTime = this.votingSessionService.addDateTimeDefaultOneMinute(
                votingSessionResource.getEndVote());
        votingSessionResource.setEndVote(newDateTime);

        var votingSessionCreated = votingSessionService.create(votingSessionResource);
        return new ResponseEntity<>(new VotingSessionResource(votingSessionCreated), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Get the Result Voting Session",
            notes = "Find Result Voting Session by Voting Session id",
            response = VotingSessionResource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return the Result Voting Session."),
            @ApiResponse(code = 404, message = VOTING_SESSION_NOT_REGISTERED),
            @ApiResponse(code = 400, message = VOTING_SESSION_NOT_CLOSED)
    })
    @GetMapping("{votingSessionId}/results")
    public ResponseEntity<Object> findResultByVotingSessionId(
            @PathVariable("votingSessionId") UUID votingSessionId) {
        var votingSession = votingSessionService.findById(votingSessionId);
        if (votingSession == null) {
            return ResponseEntity.badRequest().body(
                    new ValidationApiResource(HttpStatus.NOT_FOUND, VOTING_SESSION_NOT_REGISTERED, false)
            );
        }

        var votingSessionNotClosedError = this.votingSessionService.verifyVotingSessionNotClosed(votingSessionId);
        if (!votingSessionNotClosedError.isValid()) {
            return ResponseEntity.badRequest().body(votingSessionNotClosedError);
        }

        var countYes = this.voteService.countByVotingSessionIdAndChoice(votingSessionId, Choice.YES);
        var countNot = this.voteService.countByVotingSessionIdAndChoice(votingSessionId, Choice.NOT);

        return new ResponseEntity<>(new ResultResource(countYes, countNot), HttpStatus.OK);
    }
}
