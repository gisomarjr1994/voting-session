package com.dbc.company.gisomar.api;

import org.springframework.stereotype.Component;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;

@Component
public class ConfigurationApi {

    private static String APP = "Voting Session";
    private static String DESCRIPTION = "API for managing voting session.";
    private static String CONTACT_NAME = "Gisomar Junior";
    private static String URL_REPOSITORY_GIT = "https://gitlab.com/gisomarjr1994";
    private static String CONTACT_EMAIL = "gisomarjr1994@gmail.com";
    private static String LICENSE = "Apache License Version 2.0";

    public static ApiInfo getApiInfo(String version) {
        return new ApiInfoBuilder()
                .title(APP)
                .version(version)
                .description(DESCRIPTION)
                .contact(new Contact(CONTACT_NAME, URL_REPOSITORY_GIT, CONTACT_EMAIL))
                .license(LICENSE)
                .build();
    }

}
