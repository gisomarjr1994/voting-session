package com.dbc.company.gisomar.api.v1.resource;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
public class ValidationApiResource {

    @ApiModelProperty(notes = "Status code the error")
    private HttpStatus status;
    @ApiModelProperty(notes = "Message the error")
    private String message;
    @ApiModelProperty(notes = "Inform that the Resource is valid")
    private boolean valid;

    public ValidationApiResource(boolean valid) {
        this.valid = valid;
    }
}
