package com.dbc.company.gisomar.api.v1.resource;

import com.dbc.company.gisomar.associate.entity.Associate;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "Resource Associate for API Rest")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssociateResource {

    @ApiModelProperty(notes = "Id generate the Associate")
    private UUID id;

    @ApiModelProperty(notes = "CPF the Associate")
    private String cpf;

    public AssociateResource(Associate associate) {
        this.id = associate.getId();
        this.cpf = associate.getCpf();
    }
}
