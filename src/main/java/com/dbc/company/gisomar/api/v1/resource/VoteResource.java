package com.dbc.company.gisomar.api.v1.resource;

import com.dbc.company.gisomar.vote.entity.Vote;
import com.dbc.company.gisomar.vote.enums.Choice;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "Resource Vote for API Rest")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VoteResource {

    @ApiModelProperty(notes = "Id generate the Vote")
    private UUID id;

    @ApiModelProperty(notes = "Id the Associate")
    private UUID associateId;

    @ApiModelProperty(notes = "Id the Vote Session")
    private UUID voteSessionId;

    @ApiModelProperty(notes = "The Choice Yes or Not")
    private Choice choice;

    public VoteResource(Vote vote) {
        this.id = vote.getId();
        this.associateId = vote.getAssociate().getId();
        this.voteSessionId = vote.getVotingSession().getId();
        this.choice = vote.getChoice();
    }
}
