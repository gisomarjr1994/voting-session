package com.dbc.company.gisomar.api.v1.controller;

import com.dbc.company.gisomar.api.v1.resource.AssociateResource;
import com.dbc.company.gisomar.api.v1.resource.ValidationApiResource;
import com.dbc.company.gisomar.associate.service.AssociateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@Api(value = "associates", tags = "Associate")
@RequestMapping("/v1.0/associates/")
public class AssociateController {

    @Autowired
    private AssociateService associateService;
    private static final String ASSOCIATE_NOT_REGISTERED = "Associate is not already registered";
    private static final String ASSOCIATE_REGISTERED = "Associate is already registered";

    @ApiOperation(value = "Get the Associate",
                  notes = "Find Associate by id",
                  response = AssociateResource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return the associate."),
            @ApiResponse(code = 404, message = ASSOCIATE_NOT_REGISTERED)
    })
    @GetMapping("{id}")
    public ResponseEntity<Object> findById(@PathVariable("id") UUID id) {
        var associate = this.associateService.findById(id);
        if(associate == null) {
            return ResponseEntity.badRequest().body(
                    new ValidationApiResource(HttpStatus.NOT_FOUND, ASSOCIATE_NOT_REGISTERED, false)
            );
        }
        return new ResponseEntity<>(new AssociateResource(associate), HttpStatus.OK);
    }

    @PostMapping()
    @ApiOperation(value = "Create the Associate",
            notes = "Create Schedule by AssociateResource",
            response = AssociateResource.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Return the Associate created."),
            @ApiResponse(code = 400, message = "Associate already registered or CPF Invalid")
    })
    public ResponseEntity<Object> create(@RequestBody AssociateResource associateResource) {
        associateResource.setCpf(this.associateService.removeCharactersCPF(associateResource.getCpf()));

        var validationCPF = this.associateService.isInvalidCPF(associateResource.getCpf());
        if(!validationCPF.isValid()){
            return ResponseEntity.badRequest().body(validationCPF);
        }

        var associate = this.associateService.findByCpf(associateResource.getCpf());
        if (associate != null) {
            return ResponseEntity.badRequest().body(
                    new ValidationApiResource(HttpStatus.BAD_REQUEST, ASSOCIATE_REGISTERED, false)
            );
        }
        var associateCreated = this.associateService.create(associateResource);
        return new ResponseEntity<>(new AssociateResource(associateCreated), HttpStatus.CREATED);
    }
}
