package com.dbc.company.gisomar.api.v1.resource;

import com.dbc.company.gisomar.votingsession.entity.VotingSession;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "Resource Voting Session for API Rest")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VotingSessionResource {

    @ApiModelProperty(notes = "Id generate the Voting Session")
    private UUID id;

    @ApiModelProperty(notes = "Id of the Schedule")
    private UUID scheduleId;

    @ApiModelProperty(notes = "End the Voting Session")
    private LocalDateTime endVote;

    public VotingSessionResource(VotingSession votingSession) {
        this.id = votingSession.getId();
        this.scheduleId = votingSession.getSchedule().getId();
        this.endVote = votingSession.getEndVote();
    }
}
