package com.dbc.company.gisomar.api.v1.resource;

import com.dbc.company.gisomar.vote.enums.StatusCPFVote;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "Resource AssociateStatusResource for API Rest")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssociateStatusResource {
    StatusCPFVote status;
}
