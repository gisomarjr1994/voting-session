package com.dbc.company.gisomar.api.v1.resource;

import com.dbc.company.gisomar.schedule.entity.Schedule;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "Resource Schedule for API Rest")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScheduleResource {

    @ApiModelProperty(notes = "Id generate the Schedule")
    private UUID id;

    @ApiModelProperty(notes = "Description of the Schedule")
    private String description;

    public ScheduleResource(Schedule schedule) {
        this.id = schedule.getId();
        this.description = schedule.getDescription();
    }
}
