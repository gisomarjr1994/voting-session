package com.dbc.company.gisomar.associate.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Associate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idAssociate", unique = true, nullable = false)
    private UUID id;

    @Column(unique = true, length = 11, nullable = false)
    private String cpf;
}
