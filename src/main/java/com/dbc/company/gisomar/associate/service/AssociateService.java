package com.dbc.company.gisomar.associate.service;

import br.com.caelum.stella.validation.CPFValidator;
import br.com.caelum.stella.validation.InvalidStateException;
import com.dbc.company.gisomar.api.v1.resource.AssociateResource;
import com.dbc.company.gisomar.api.v1.resource.ValidationApiResource;
import com.dbc.company.gisomar.associate.entity.Associate;
import com.dbc.company.gisomar.associate.repository.AssociateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class AssociateService {

    @Autowired
    private AssociateRepository associateRepository;
    private static final String CPF_IS_INVALID = "CPF is Invalid";

    public Associate findById(UUID id) {
        var associate = associateRepository.findById(id);
        if(!associate.isPresent()) {
            return null;
        }
        return associate.get();
    }

    public Associate create(AssociateResource associateResource) {
        Associate associate = new Associate();
        associate.setCpf(associateResource.getCpf());
        return this.associateRepository.save(associate);
    }

    public String removeCharactersCPF(String cpf) {
        return cpf.replaceAll("[.-]","");
    }

    public ValidationApiResource isInvalidCPF(String cpf) {
        CPFValidator cpfValidator = new CPFValidator();
        try {
            cpfValidator.assertValid(cpf);
        } catch (InvalidStateException invalidStateException) {
            return new ValidationApiResource(HttpStatus.BAD_REQUEST, CPF_IS_INVALID,false);
        }
        return new ValidationApiResource(true);
    }

    public Associate findByCpf(String cpf) {
        var associate = associateRepository.findByCpf(cpf);
        if(!associate.isPresent()) {
            return null;
        }
        return associate.get();
    }
}
