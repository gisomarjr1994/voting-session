package com.dbc.company.gisomar.associate.service;

import com.dbc.company.gisomar.api.v1.resource.AssociateResource;
import com.dbc.company.gisomar.api.v1.resource.ValidationApiResource;
import com.dbc.company.gisomar.associate.entity.Associate;
import java.util.UUID;

public interface IAssociateService {

    /**
     * Find the Associate by {@link UUID id}
     * @param id
     * @return
     */
    Associate findById(UUID id);

    /**
     * Create the Associate by {@link AssociateResource associateResource}
     * @param associateResource
     * @return
     */
    Associate create(AssociateResource associateResource);

    /**
     * Verify CPF is invalid {@link String cpf}
     * @param cpf
     * @return
     */
    ValidationApiResource isInvalidCPF(String cpf);

    /**
     * Remove characters of the CPF {@link String cpf}
     * @param cpf
     * @return
     */
    String removeCharactersCPF(String cpf);

    /**
     * Find the CPF for the Associate {@link String cpf}
     * @param cpf
     * @return
     */
    Associate findByCpf(String cpf);
}
