package com.dbc.company.gisomar.associate.repository;
import com.dbc.company.gisomar.associate.entity.Associate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface AssociateRepository extends JpaRepository<Associate, UUID> {

    Optional<Associate> findByCpf(@Param("cpf") String cpf);
}
