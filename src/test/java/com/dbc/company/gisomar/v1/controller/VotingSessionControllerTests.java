package com.dbc.company.gisomar.v1.controller;

import com.bazaarvoice.jolt.JsonUtils;
import com.dbc.company.gisomar.api.v1.controller.VotingSessionController;
import com.dbc.company.gisomar.api.v1.resource.ValidationApiResource;
import com.dbc.company.gisomar.api.v1.resource.VotingSessionResource;
import com.dbc.company.gisomar.schedule.entity.Schedule;
import com.dbc.company.gisomar.schedule.service.ScheduleService;
import com.dbc.company.gisomar.vote.enums.Choice;
import com.dbc.company.gisomar.vote.service.VoteService;
import com.dbc.company.gisomar.votingsession.entity.VotingSession;
import com.dbc.company.gisomar.votingsession.service.VotingSessionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import java.time.LocalDateTime;
import java.util.UUID;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = VotingSessionController.class)
class VotingSessionControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VotingSessionService votingSessionService;

    @MockBean
    private VotingSession votingSession;

    @MockBean
    private ScheduleService scheduleService;

    @MockBean
    private VoteService voteService;

    @Mock
    private Schedule schedule;

    @Mock
    private VotingSessionResource votingSessionResource;

    @Mock
    private ValidationApiResource validationApiResource;

    private static final String URL = "/v1.0/voting-sessions/";

    @BeforeEach
    void setup() {
        votingSession = new VotingSession();
        schedule = new Schedule();
        schedule.setDescription("test");
        schedule.setId(UUID.randomUUID());
        votingSession.setSchedule(schedule);
        votingSession.setId(UUID.randomUUID());
        votingSessionResource = new VotingSessionResource(votingSession);
        votingSession.setEndVote(LocalDateTime.now());
        validationApiResource = new ValidationApiResource(true);
    }

    @Test
    void findById() throws Exception {
        when(votingSessionService.findById(votingSession.getId())).thenReturn(votingSession);
        RequestBuilder request = MockMvcRequestBuilders
                .get(URL.concat(votingSession.getId().toString()))
                .param("id", votingSession.getId().toString())
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request).andExpect(status().isOk());
    }

    @Test
    void create() throws Exception {

        when(scheduleService.findById(votingSessionResource.getScheduleId())).thenReturn(schedule);
        when(votingSessionService.addDateTimeDefaultOneMinute(votingSessionResource.getEndVote())).thenReturn(LocalDateTime.now());
        when(votingSessionService.create(any(VotingSessionResource.class))).thenReturn(votingSession);

        RequestBuilder request = MockMvcRequestBuilders
                .post(URL)
                .content(JsonUtils.toJsonString(votingSessionResource))
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request).andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", notNullValue()))
                .andExpect(jsonPath("$.scheduleId", is(votingSessionResource.getScheduleId().toString())))
                .andExpect(jsonPath("$.endVote", notNullValue()));
    }

    @Test
    void findResultByVotingSessionId() throws Exception {
        when(votingSessionService.findById(votingSession.getId())).thenReturn(votingSession);
        when(votingSessionService.verifyVotingSessionNotClosed(votingSession.getId())).thenReturn(validationApiResource);
        when(voteService.countByVotingSessionIdAndChoice(votingSession.getId(), Choice.YES)).thenReturn(10);
        when(voteService.countByVotingSessionIdAndChoice(votingSession.getId(), Choice.NOT)).thenReturn(15);

        RequestBuilder request = MockMvcRequestBuilders
                .get(URL.concat(votingSession.getId().toString()).concat("/results"))
                .param("id", votingSession.getId().toString())
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request).andExpect(status().isOk())
                .andExpect(jsonPath("$.countYes", is(10)))
                .andExpect(jsonPath("$.countNot", is(15)));
    }
}
