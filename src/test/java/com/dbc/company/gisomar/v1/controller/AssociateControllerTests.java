package com.dbc.company.gisomar.v1.controller;

import com.dbc.company.gisomar.api.v1.controller.AssociateController;
import com.dbc.company.gisomar.api.v1.resource.AssociateResource;
import com.dbc.company.gisomar.api.v1.resource.ValidationApiResource;
import com.dbc.company.gisomar.associate.entity.Associate;
import com.dbc.company.gisomar.associate.service.AssociateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import java.util.UUID;
import com.bazaarvoice.jolt.JsonUtils;

@WebMvcTest(controllers = AssociateController.class)
class AssociateControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AssociateService associateService;

    @Mock
    private Associate associate;

    @Mock
    private AssociateResource associateResource;

    @Mock
    private ValidationApiResource validationApiResource;

    private static final String URL = "/v1.0/associates/";

    @BeforeEach
    void setup() {
        associate = new Associate();
        associate.setCpf("03562364040");
        UUID uuid = UUID.randomUUID();
        associate.setId(uuid);
        associateResource = new AssociateResource(associate);
        validationApiResource = new ValidationApiResource(true);
    }

    @Test
    void findById() throws Exception {
        when(associateService.findById(associate.getId())).thenReturn(associate);
        given(associateService.findByCpf(associate.getCpf())).willReturn(associate);

        RequestBuilder request = MockMvcRequestBuilders
                .get(URL.concat(associate.getId().toString()))
                .param("id", associate.getId().toString())
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request).andExpect(status().isOk());
    }

    @Test
    void create() throws Exception {
        when(associateService.removeCharactersCPF(associate.getCpf())).thenReturn(associate.getCpf());
        when(associateService.isInvalidCPF(associate.getCpf())).thenReturn(validationApiResource);
        when(associateService.create(any(AssociateResource.class))).thenReturn(associate);

        RequestBuilder request = MockMvcRequestBuilders
                .post(URL)
                .content(JsonUtils.toJsonString(associateResource))
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request).andExpect(status().isCreated());
    }
}
