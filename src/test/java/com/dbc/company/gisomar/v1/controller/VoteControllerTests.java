package com.dbc.company.gisomar.v1.controller;

import com.bazaarvoice.jolt.JsonUtils;
import com.dbc.company.gisomar.api.v1.controller.VoteController;
import com.dbc.company.gisomar.api.v1.resource.ValidationApiResource;
import com.dbc.company.gisomar.api.v1.resource.VoteResource;
import com.dbc.company.gisomar.associate.entity.Associate;
import com.dbc.company.gisomar.associate.service.AssociateService;
import com.dbc.company.gisomar.vote.entity.Vote;
import com.dbc.company.gisomar.vote.enums.Choice;
import com.dbc.company.gisomar.vote.service.VoteService;
import com.dbc.company.gisomar.votingsession.entity.VotingSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import java.util.UUID;

@WebMvcTest(controllers = VoteController.class)
class VoteControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VoteService voteService;

    @MockBean
    private AssociateService associateService;

    @Mock
    private Vote vote;

    @Mock
    private VoteResource voteResource;

    @Mock
    private VotingSession votingSession;

    @Mock
    private Associate associate;

    @Mock
    private ValidationApiResource validationApiResource;

    private static final String URL = "/v1.0/votes/";

    @BeforeEach
    void setup() {
        vote = new Vote();
        vote.setId(UUID.randomUUID());
        votingSession = new VotingSession();
        votingSession.setId(UUID.randomUUID());
        associate.setId(UUID.randomUUID());
        associate.setCpf("03562364040");
        vote.setAssociate(associate);
        vote.setChoice(Choice.YES);
        vote.setVotingSession(votingSession);
        vote.setAssociate(associate);
        voteResource = new VoteResource(vote);
        validationApiResource = new ValidationApiResource(true);
    }

    @Test
    void create() throws Exception {
        when(voteService.existsVotingSession(voteResource.getVoteSessionId())).thenReturn(validationApiResource);
        when(voteService.existsAssociate(associate.getId())).thenReturn(validationApiResource);
        when(associateService.findById(associate.getId())).thenReturn(associate);
        when(voteService.isAssociateUnableVote(associate.getCpf())).thenReturn(validationApiResource);
        when(voteService.isVotingSessionClosed(votingSession.getId())).thenReturn(validationApiResource);
        when(voteService.isAssociateAlreadyVoted(associate.getId(), votingSession.getId())).thenReturn(validationApiResource);
        when(voteService.create(any(VoteResource.class))).thenReturn(vote);

        RequestBuilder request = MockMvcRequestBuilders
                .post(URL)
                .content(JsonUtils.toJsonString(voteResource))
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request).andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", notNullValue()))
                .andExpect(jsonPath("$.voteSessionId", is(vote.getVotingSession().getId().toString())))
                .andExpect(jsonPath("$.choice", is(vote.getChoice().toString())));;
    }
}
