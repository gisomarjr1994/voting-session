package com.dbc.company.gisomar.v1.controller;

import com.bazaarvoice.jolt.JsonUtils;
import com.dbc.company.gisomar.api.v1.controller.ScheduleController;
import com.dbc.company.gisomar.api.v1.resource.ScheduleResource;
import com.dbc.company.gisomar.schedule.entity.Schedule;
import com.dbc.company.gisomar.schedule.service.ScheduleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import java.util.UUID;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = ScheduleController.class)
class ScheduleControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ScheduleService scheduleService;

    @Mock
    private Schedule schedule;

    @Mock
    private ScheduleResource scheduleResource;

    private static final String URL = "/v1.0/schedules/";

    @BeforeEach
    void setup() {
        schedule = new Schedule();
        schedule.setDescription("New Schedule");
        UUID uuid = UUID.randomUUID();
        schedule.setId(uuid);
        scheduleResource = new ScheduleResource(schedule);
    }

    @Test
    void findById() throws Exception {
        when(scheduleService.findById(schedule.getId())).thenReturn(schedule);
        RequestBuilder request = MockMvcRequestBuilders
                .get(URL.concat(schedule.getId().toString()))
                .param("id", schedule.getId().toString())
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request).andExpect(status().isOk());
    }

    @Test
    void create() throws Exception {
        when(scheduleService.findById(schedule.getId())).thenReturn(schedule);
        when(scheduleService.create(any(ScheduleResource.class))).thenReturn(schedule);

        RequestBuilder request = MockMvcRequestBuilders
                .post(URL)
                .content(JsonUtils.toJsonString(scheduleResource))
                .contentType(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(request).andExpect(status().isCreated());
    }
}
